---
title: "moveo-group draw&guess game"
disqus: hackmd
---

# Draw&Guess By Morad

[Site preview](https://drawnguess.netlify.app/)

## Table of Contents

[TOC]

## Beginners Guide

A game that can be played by multiple players!

1. Create/Join a new room
2. Invite your friends to the room
3. Choose a word and draw it while your friends watching it live
4. The player who guesses right first gets points
5. Accumlate as much as you can.

## User story

```gherkin=
Feature: Guess the word

  # The first example has two steps
  Scenario: Maker starts a game
    When the Maker starts a game
    Then the Maker waits for a Breaker to join
    Then the Maker chooses a word and draw it
    Then Breaker tries to guess in order to get points
```

## Development flow

```sequence
Note left of Frontend: Player draws
Frontend->Backend: Sending path in realtime using socket.io
Note right of Backend: Processing
Backend-->Frontend: get emits to players in the room!
Note left of Frontend: All of the room
Note left of Frontend: players watch
```

> Read more about socket.io here:
> https://socket.io/get-started/chat

> Excuses don't make results . [name=Not trying to make one, but please go over the 'Yet to come section']

## Yet to come

My plan was to create a realtime multiplayer drawing app, using socket.io which will help broadcasting specific content to player that joined some room.

14/03 - Recieved my task from Gal and immediately started working on it.

15/03 - My spouse recieved her success grade in Occupational therapy - professional licensing. Due to celebrations could'nt work on it.

17/03 is the last day that i worked on the task.

Due to lack of time because of many reasons, i installed firebase and wanted to convert it to a game that the result would be shared only on demand and not in realtime, but its already Sunday and i want to share the results with you already.

Thanks for reading me and for understanding the delighting and unexpected circumstances.

## Appendix and FAQ

:::info
**Frontend**
_ReactJs_
_MaterialUi_
_socket.io client_
_(Firebase )_
**Backend**
_socket.io_
_NodeJS_
:::

###### tags: `Games` `Multiplayer` `Documentation`
