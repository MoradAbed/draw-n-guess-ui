import logo from "./logo.svg";
import "./App.css";
import DrawView from "./components/drawView/DrawView";
import { useView } from "./context/ActiveView";
import WelcomeView from "./components/welcomeView/WelcomeView";
import { toast } from "react-toastify";

function App() {
  const { activeView } = useView();
  toast.configure({
    position: "top-right",
    autoClose: 1500,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
  });
  return (
    <div className="App">
      {activeView === "welcome" ? <WelcomeView /> : <DrawView />}
    </div>
  );
}

export default App;
