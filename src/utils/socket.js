import { io } from "socket.io-client";
const socket = io("https://draw-n-guess-server.herokuapp.com/");

export default socket;