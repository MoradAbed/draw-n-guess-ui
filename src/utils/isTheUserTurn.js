
const isTheUserTurn = (manager) => {
  const userName = JSON.parse(sessionStorage.getItem("user")).userName;
  console.log(manager)
  if (manager?.playerDrawing !== userName) {
    return false;
  }
  return true;
};

export default isTheUserTurn;
