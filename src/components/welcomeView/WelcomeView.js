import {
  Alert,
  Button,
  Grid,
  Paper,
  Stack,
  TextField,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { Socket } from "socket.io-client";
import doodle from "../../assets/doodle.gif";
import { useView } from "../../context/ActiveView";
import socket from "../../utils/socket";
import { v4 as uuid } from "uuid";
import { toast, ToastContainer } from "react-toastify";
import firebase from "firebase";
import { db } from "../../_firebase/index";
import { useGameManager } from "../../context/GameManager";

const WelcomeView = () => {
  const { setActiveView } = useView();
  const { setGameManagement, gameManagement } = useGameManager();
  const theme = useTheme();
  const matchDownMD = useMediaQuery(theme.breakpoints.down("md"));
  const [room, setRoom] = useState({
    isNew: false,
  });
  const [joinDetails, setJoinDetails] = useState({ userName: "", roomUid: "" });

  const handleJoinDetailsChange = (e) => {
    setJoinDetails({ ...joinDetails, [e.target.name]: e.target.value });
  };
  const checkDisabledEnterButton = () => {
    if (
      room.isNew &&
      joinDetails.userName !== "" &&
      joinDetails.userName.length > 0
    ) {
      return false;
    } else if (
      !room.isNew &&
      joinDetails.userName !== "" &&
      joinDetails.userName.length > 0 &&
      joinDetails.roomUid.length > 0 &&
      joinDetails.roomUid !== ""
    ) {
      return false;
    }
    return true;
  };
  const handleEnterRoom = () => {
    sessionStorage.setItem("connected", true);
    let roomUid;
    if ((joinDetails.roomUid === "" || !joinDetails.roomUid) && room.isNew) {
      try {
        const time = firebase.firestore.Timestamp.now().seconds;
        const roomRef = db.collection("rooms").doc();
        roomRef
          .set({
            createdBy: joinDetails.userName,
            createdAt: time,
          })
          .then(() => {
            const { id } = roomRef;
            setRoom({ ...room, uid: id });
          })
          .catch((err) => {
            console.log(err);
          });
        roomUid = roomRef.id;
        console.log(roomUid);
        console.log(room);
        setGameManagement({
          ...gameManagement,
          playerDrawing: joinDetails.userName,
        });
        // socket.emit("whosTurn", joinDetails.userName);
      } catch (error) {
        console.log(error);
      }
    } else {
      const temp = { ...room, uid: joinDetails.roomUid };
      roomUid = joinDetails.roomUid;
      setRoom(temp);
      // socket.emit("joinRoom", room, (msg) => {
      //   console.log(msg);
      // });
    }
    socket.emit(
      "joinRoom",
      room.isNew,
      roomUid,
      joinDetails.userName,
      (msg) => {
        sessionStorage.setItem("whosTurn", msg.playerDrawing);
        setGameManagement({
          ...gameManagement,
          playerDrawing: msg.playerDrawing,
          roomUid,
        });
      }
    );

    sessionStorage.setItem(
      "user",
      JSON.stringify({
        roomUid,
        userName: joinDetails.userName,
        creator: room.isNew,
      })
    );
    setActiveView("draw");
  };
  return (
    <>
      {room?.uid?.length > 0 ? (
        <Alert severity="success" color="info">
          Joined Succesfully to room {`${room}`}
        </Alert>
      ) : null}
      <Stack direction="row" justifyContent="center">
        {/* <ToastContainer /> */}
        <Stack direction="column" alignItems="center">
          <Typography
            sx={{
              fontSize: "5vw",
            }}
            color="cadetblue"
          >
            Welcome to Draw&amp;Guess
          </Typography>
          <Grid
            spacing={2}
            sx={{ display: "flex", alignItems: "space-around" }}
            container
          >
            {/* <Box component="form"> */}
            <Grid item xs={12} md={8} sm={8} lg={8}>
              <TextField
                value={joinDetails.userName}
                onChange={(e) => handleJoinDetailsChange(e)}
                fullWidth
                variant="outlined"
                label="User name"
                name="userName"
              />
            </Grid>
            {!room.isNew ? (
              <Grid item xs={12} md={8} sm={8} lg={8}>
                <TextField
                  value={joinDetails.roomUid}
                  onChange={(e) => handleJoinDetailsChange(e)}
                  fullWidth
                  name="roomUid"
                  variant="outlined"
                  label="Room ID"
                />
              </Grid>
            ) : null}
            <Grid item xs={12} md={8} sm={8} lg={8}>
              <Button
                onClick={() => {
                  setRoom({ ...room, isNew: true });
                  setJoinDetails({ ...joinDetails, roomUid: "" });
                }}
                sx={{ mb: 2 }}
                fullWidth
                variant={room.isNew ? "contained" : "outlined"}
              >
                New room
              </Button>
              <Button
                onClick={() => {
                  setRoom({ ...room, isNew: false });
                }}
                fullWidth
                variant={!room.isNew ? "contained" : "outlined"}
                //   variant="outlined"
              >
                Join room
              </Button>
              <Button
                variant="contained"
                color="success"
                sx={{
                  mt: 2,
                }}
                onClick={() => {
                  handleEnterRoom();
                }}
                disabled={checkDisabledEnterButton()}
              >
                Enter
              </Button>
            </Grid>
          </Grid>
          <Grid item xs={6} md={4} sm={4} lg={4}></Grid>
        </Stack>
        {matchDownMD ? null : <img src={doodle} />}
      </Stack>
    </>
  );
};

export default WelcomeView;
