import { Button, CardActions, CardMedia } from "@mui/material";
import React from "react";
import { useCanvas } from "./CanvasContext";

export const CanvasViewActions = () => {
  const { clearCanvas, canvasRef } = useCanvas();

  return (
    <CardActions sx={{ justifyContent: "center", mt: 10 }}>
      <Button
        variant="contained"
        onClick={() => {
          const url = canvasRef.current.toDataURL();
          console.log(url);
        }}
      >
        Share
      </Button>
      <Button sx={{ width: "25%" }} onClick={clearCanvas} variant="outlined">
        clear
      </Button>
    </CardActions>
  );
};
