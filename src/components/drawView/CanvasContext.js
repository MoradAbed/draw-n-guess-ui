import React, { useContext, useEffect, useRef, useState } from "react";
import { useView } from "../../context/ActiveView";
import { useGameManager } from "../../context/GameManager";
import socket from "../../utils/socket";
import { db } from "../../_firebase";

const CanvasContext = React.createContext();

export const CanvasProvider = ({ children }) => {
  const { activeView } = useView();
  const { gameManagement, setGameManagement } = useGameManager();
  const [isDrawing, setIsDrawing] = useState(false);
  const [colorPicked, setColorPicked] = useState("#ff0000");
  const [pathDrawnByPlayer, setPathDrawnByPlayer] = useState([]);
  const canvasRef = useRef(null);
  const contextRef = useRef(null);

  const prepareCanvas = () => {
    const canvas = canvasRef.current;
    canvas.width = window.innerWidth * 2;
    canvas.height = window.innerHeight * 2;
    canvas.style.width = `${window.innerWidth}px`;
    canvas.style.height = `${window.innerHeight}px`;

    const context = canvas.getContext("2d");
    context.scale(2, 2);
    context.lineCap = "round";
    context.strokeStyle = colorPicked;
    context.lineWidth = 5;
    contextRef.current = context;
  };

  useEffect(() => {
    if (activeView === "draw") {
      const canvas = canvasRef.current;
      const context = canvas.getContext("2d");
      context.strokeStyle = colorPicked;
      context.current = context;
    }
  }, [colorPicked, activeView]);
  const startDrawing = ({ nativeEvent }) => {
    // const { offsetX, offsetY } = nativeEvent;
    let tempDrawingArr = [...pathDrawnByPlayer];
    let offsetX, offsetY;
    offsetX = nativeEvent.offsetX;
    offsetY = nativeEvent.offsetY;
    tempDrawingArr.push({
      color: colorPicked,
      cords: [[offsetX, offsetY]],
    });
    setPathDrawnByPlayer(tempDrawingArr);
    // the active player is who emits to the other players
    contextRef.current.beginPath();
    contextRef.current.moveTo(offsetX, offsetY);
    setIsDrawing(true);
  };

  let temp = [...pathDrawnByPlayer];
  const finishDrawing = () => {
    contextRef.current.closePath();
    temp.length = 0;
    const { roomUid, userName, creator } = JSON.parse(
      sessionStorage.getItem("user")
    );
    if (
      gameManagement?.playerDrawing &&
      gameManagement.playerDrawing === userName
    ) {
      // only if its the turn of the user, share the drawing
      if (gameManagement.selectedWord) {
        const roomRef = db.collection("rooms").doc(roomUid);
        let roundRef;
        if (gameManagement && gameManagement.roundId) {
          roundRef = roomRef.collection("rounds").doc(gameManagement.roundId);
        } else {
          roundRef = roomRef.collection("rounds").doc();
        }
        const url = canvasRef.current.toDataURL();
        // const id = roundRef.id;
        roundRef
          .set({
            playerDrawing: userName,
            img: url,
            correctAnswer: gameManagement.selectedWord,
          })
          .then(() => {
            socket.emit(
              "finishedDrawing",
              roomUid,
              roundRef.id,
              (cbRoundData) => {
                console.log(cbRoundData);
              }
            );
          })
          .catch((err) => {
            console.log(err);
          });
        setGameManagement({
          ...gameManagement,
          roundId: roundRef.id,
          correctAnswer: gameManagement.selectedWord,
        });
      } else {
        // the player hasnt selected a word
        window.alert("select a word please");
      }
      // if the player is drawing then emit the round id
    }
    setIsDrawing(false);
  };

  const draw = ({ nativeEvent }) => {
    if (!isDrawing) {
      return;
    }
    const { offsetX, offsetY } = nativeEvent;
    contextRef.current.lineTo(offsetX, offsetY);
    // console.log(nativeEvent);
    temp[temp.length - 1].cords.push([offsetX, offsetY]);
    setPathDrawnByPlayer(temp);
    // tempDrawingArr[tempDrawingArr.length - 1].cords.push([offsetX, offsetY]);
    // push to the last existing array
    contextRef.current.stroke();
  };

  const clearCanvas = () => {
    const canvas = canvasRef.current;
    const context = canvas.getContext("2d");
    context.fillStyle = "white";
    context.fillRect(0, 0, canvas.width, canvas.height);
    setPathDrawnByPlayer([]);
  };

  return (
    <CanvasContext.Provider
      value={{
        canvasRef,
        contextRef,
        prepareCanvas,
        startDrawing,
        finishDrawing,
        clearCanvas,
        draw,
        setColorPicked,
        colorPicked,
        isDrawing,
      }}
    >
      {children}
    </CanvasContext.Provider>
  );
};

export const useCanvas = () => useContext(CanvasContext);
