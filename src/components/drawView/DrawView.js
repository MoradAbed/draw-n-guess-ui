import React, { useEffect, useRef, useState } from "react";
import { Canvas } from "./Canvas";
import { CanvasViewActions } from "./CanvasViewActions";
import Card from "@mui/material/Card";
import { useCanvas } from "./CanvasContext";
import {
  Box,
  Button,
  CardActions,
  CardMedia,
  Chip,
  Dialog,
  DialogContent,
  Drawer,
  Grid,
  IconButton,
  Modal,
  Paper,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
  useTheme,
} from "@mui/material";
import { MenuOutlined, LogoutOutlined } from "@mui/icons-material";
import words from "random-words";
import modes from "../../constants/modes";
import { useView } from "../../context/ActiveView";
import socket from "../../utils/socket";
import { useGameManager } from "../../context/GameManager";
import { db } from "../../_firebase";

const DrawView = () => {
  const theme = useTheme();
  const { setColorPicked, colorPicked, draw, startDrawing, finishDrawing } =
    useCanvas();
  const { activeView, setActiveView } = useView();
  const { gameManagement, setGameManagement } = useGameManager();
  const [randomWords, setRandomWord] = useState();
  const [isWaitingForDrawing, setWaitForDrawingView] = useState({
    state: true,
  });
  const [isNewVersionOfDrawing, setNewVersionOfDrawingWasCaptured] =
    useState(false);
  const [selectedWord, setSelectedWord] = useState();
  const [gameResults, setGameResults] = useState([
    { num: 1, answeredCorrectly: true, gussedBy: "player 2", duration: "51s" },
  ]);
  const [isDrawerOpen, setDrawerIsOpen] = useState(false);
  const changeColor = (e) => {
    setColorPicked(e.target.value);
  };

  const handleGenerateNewRandomWord = () => {
    const random = words({ exactly: 3, maxLength: modes.easy.max });
    setRandomWord(random);
  };
  const handleWordSelect = (word) => {
    setSelectedWord(word);
    setGameManagement({ ...gameManagement, selectedWord: word });
  };
  const handleChangeView = (view) => {
    setActiveView(view);
    console.log(view, activeView);
  };
  useEffect(() => {
    handleGenerateNewRandomWord();
  }, []);

  useEffect(() => {
    if (gameManagement) {
      if (
        JSON.parse(sessionStorage.getItem("user")).userName ===
        gameManagement.playerDrawing
      ) {
        setWaitForDrawingView({ ...isWaitingForDrawing, state: false });
      }
      setWaitForDrawingView({ ...isWaitingForDrawing, state: true });
    }
  }, [gameManagement]);
  socket.on("playerFinishedDrawing", (res) => {
    setGameManagement({ ...gameManagement, roundId: res.roundId });
    sessionStorage.setItem(
      "roundData",
      JSON.stringify({
        correctAnswer: res.correctAnswer,
        roundId: res.roundId,
        playerDrawing: res.playerDrawing,
      })
    );
    setNewVersionOfDrawingWasCaptured(true);
  });

  const retrieveNewPic = async () => {
    const ref = await db
      .collection("rooms")
      .doc(gameManagement.roomUid)
      .collection("rounds")
      .doc(gameManagement.roundId)
      .get();
    if (ref.exists) {
      // ref.data();
      console.log(ref.data());
      setWaitForDrawingView({ ...isWaitingForDrawing, img: ref.data().img });
    }
  };

  const handleClose = () => {
    setNewVersionOfDrawingWasCaptured({
      ...isNewVersionOfDrawing,
      state: false,
    });
  };
  useEffect(() => {
    if (isNewVersionOfDrawing) {
      retrieveNewPic();
    }
  }, [isNewVersionOfDrawing]);
  return (
    <>
      <IconButton
        onClick={() => setDrawerIsOpen(true)}
        sx={{ position: "fixed", right: 0, top: 0 }}
      >
        <MenuOutlined color="success" />
      </IconButton>
      <Grid container>
        <Grid item xs={12} md={8} lg={8}>
          <Card
            elevation={8}
            sx={{
              // width: "60vw",
              height: "60vh",
              m: 3,
            }}
          >
            <Canvas />
            {/* <CardActions></CardActions> */}
          </Card>
          <Stack justifyContent="space-evenly" direction="row">
            <Stack direction="column">
              <Typography>Choose a word</Typography>
              <Stack justifyContent="center" m={2} spacing={1} direction="row">
                {!randomWords
                  ? null
                  : randomWords.map((word, index) => (
                      <Chip
                        onClick={() => handleWordSelect(word)}
                        label={word}
                        key={index.toString()}
                        color={selectedWord === word ? "primary" : "default"}
                        variant={
                          selectedWord === word ? "contained" : "outlined"
                        }
                      />
                    ))}
              </Stack>
            </Stack>
            <Stack gap={2} alignItems="center" direction="column">
              <Typography>Change the color</Typography>
              <input
                style={{
                  width: "8vw",
                  border: "none",
                  backgroundColor: colorPicked,
                  boxShadow: theme.shadows[4],
                }}
                type="color"
                defaultValue={colorPicked}
                onInput={changeColor}
              />
            </Stack>
          </Stack>
          <Button onClick={handleGenerateNewRandomWord} variant="outlined">
            Shuffle a new word
          </Button>
        </Grid>
        <Grid mt={2} item xs={12} md={4} lg={4}>
          {gameManagement && gameManagement.playerDrawing ? (
            <Typography>
              {gameManagement.playerDrawing} is drawing {selectedWord}
            </Typography>
          ) : null}
          <TableContainer sx={{ maxHeight: "55vh", overflowY: "auto" }}>
            <Table stickyHeader>
              <TableHead>
                <TableRow>
                  <TableCell>#</TableCell>
                  <TableCell>Answered correctly</TableCell>
                  <TableCell>Total time</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {[1, 2, 3, 4, 5, 6].map((value) => (
                  <TableRow key={value}>
                    <TableCell>{gameResults[0].num}</TableCell>
                    <TableCell>{`${gameResults[0].answeredCorrectly} (${gameResults[0].gussedBy})`}</TableCell>
                    <TableCell>{gameResults[0].duration}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <CanvasViewActions />
          {/* </Box> */}
        </Grid>
      </Grid>
      <Dialog
        onClose={handleClose}
        component={Paper}
        open={isNewVersionOfDrawing.state}
      >
        <DialogContent>
          <img
            width="100%"
            src={
              isWaitingForDrawing.state && isWaitingForDrawing.img
                ? isWaitingForDrawing.img
                : null
            }
          />
        </DialogContent>
      </Dialog>
      <Drawer
        anchor="right"
        variant="temporary"
        open={isDrawerOpen}
        onClose={() => setDrawerIsOpen(false)}
        sx={{ width: "20vw" }}
      >
        <Box>
          Your room ID is:
          <Typography>
            {JSON.parse(sessionStorage.getItem("user")).roomUid}
          </Typography>
          Your user name is:
          <Typography>
            {JSON.parse(sessionStorage.getItem("user")).userName}
          </Typography>
          <Tooltip title="Exit">
            <IconButton
              onClick={() => {
                // sessionStorage.removeItem("connected");
                // sessionStorage.removeItem("user");
                sessionStorage.clear();
                handleChangeView("welcome");
              }}
            >
              <LogoutOutlined />
            </IconButton>
          </Tooltip>
        </Box>
      </Drawer>
    </>
  );
};

export default DrawView;
