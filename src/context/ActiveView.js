import React, { createContext, useContext, useMemo, useState } from "react";

const ActiveViewContext = createContext();

export const ViewProvider = ({ children }) => {
  const [activeView, setActiveView] = useState(
    sessionStorage.getItem("user") ? "draw" : "welcome"
  );
  const activeViewData = useMemo(
    () => ({ activeView, setActiveView }),
    [activeView, setActiveView]
  );

  return (
    <ActiveViewContext.Provider value={activeViewData}>
      {children}
    </ActiveViewContext.Provider>
  );
};
export const useView = () => useContext(ActiveViewContext);
