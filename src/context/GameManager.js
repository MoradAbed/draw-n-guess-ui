import React, { createContext, useContext, useMemo, useState } from "react";

const GameManagementContext = createContext();

export const GameManager = ({ children }) => {
  const [gameManagement, setGameManagement] = useState({});
  const gameManagmenetData = useMemo(
    () => ({ gameManagement, setGameManagement }),
    [gameManagement, setGameManagement]
  );

  return (
    <GameManagementContext.Provider value={gameManagmenetData}>
      {children}
    </GameManagementContext.Provider>
  );
};
export const useGameManager = () => useContext(GameManagementContext);
