const modes = {
  easy: {
    min: 3,
    max: 5,
  },
  meduim: {
    min: 6,
    max: 8,
  },
  hard: {
    min: 8,
  },
};

export default modes;
